import http
import json
import os

from flask import Flask, render_template, request
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

url = os.environ.get('DJAPI_URL', 'https://djapi.autoipacket.com')


@app.route('/')
@cross_origin()
def index():
    if 'auth' in request.args:
        companies = http.client.HTTPSConnection(url[url.index('//') + 2:])
        companies.request('GET', '/v1/store/company/', headers={'Authorization': f'Token {request.args["auth"]}'})
        response = companies.getresponse()
        company_data = json.loads(response.read())
        company_list = company_data['results']
        while company_data['next'] is not None:
            companies.request('GET', company_data['next'][len(url):],
                              headers={'Authorization': f'Token {request.args["auth"]}'})
            response = companies.getresponse()
            company_data = json.loads(response.read())
            company_list.extend(company_data['results'])
        companies.request('GET', '/v1/users/company/stores/AutoIPacket', headers={'Authorization': f'Token {request.args["auth"]}'})
        response = companies.getresponse()
        store_data = json.loads(response.read())
        store_list = store_data['results']
        companies.request('GET', '/v1/store/dms/', headers={'Authorization': f'Token {request.args["auth"]}'})
        response = companies.getresponse()
        dms_data = json.loads(response.read())
        dms_list = dms_data['results']
        companies.request('GET', '/v1/store/aip-market/', headers={'Authorization': f'Token {request.args["auth"]}'})
        response = companies.getresponse()
        market_data = json.loads(response.read())
        market_list = market_data['results']
        return render_template('index.html', url=url, auth_token=request.args['auth'], companies=company_list,
                               stores=store_list, dms_list=dms_list, market_list=market_list)
    else:
        return render_template('login.html', url=url)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
