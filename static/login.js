async function login(url) {
    let loginURL = url + '/v1/users/auth/login/';
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;

    let formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 404) {

        } else if (request.status === 200) {
            let data = JSON.parse(request.responseText);
            window.location.replace("/?auth=" + data.token);
        }
    };
    request.open("POST", loginURL);
    request.send(formData);
}
