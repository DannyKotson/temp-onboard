function updateStores(url, token) {
    let select = document.getElementById("company-select");
    let storeList = document.getElementById("store-list");

    let name = select.options[select.selectedIndex].text;
    let storeURL = url + '/v1/users/company/stores/' + name;
    storeList.innerHTML = "";

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 404) {

        } else if (request.status === 200) {
            let data = JSON.parse(request.responseText);
            for(let i=0; i < data['count']; i++) {
                let storeType = ""
                if(data['results'][i]['store_type_id'] == 1) {
                    storeType = "New"
                }
                else if(data['results'][i]['store_type_id'] == 2) {
                    storeType = "Pre-owned"
                }
                else if(data['results'][i]['store_type_id'] == 3) {
                    storeType = "Museum"
                }
                else if(data['results'][i]['store_type_id'] == 6) {
                    storeType = "Wholesale"
                }
                let storeName = data['results'][i]['store_name'] + " " + storeType;
                let storeID = data['results'][i]['company_store_id'];
                let storePhone = data['results'][i]['store_phone'];
                let emailRequest = new XMLHttpRequest();
                emailRequest.onload = () => {
                    if (emailRequest.status === 200) {
                        let email = JSON.parse(emailRequest.responseText);
                        storeList.innerHTML += '\n' +
                            '            <span>' + storeName + '</span>\n' +
                            '            <br />' +
                            '            <label for="email-address-' + storeID + '">Email Address: </label>' +
                            '            <input type="text" name="email-address-' + storeID + '" id="email-address-' + storeID + '" value="' + email['email'] + '" />' +
                            '            <button onclick="sendConfirmAvail(\'' + url + '\', \'' + token + '\', ' + storeID + ')">Confirm Availability</button>\n' +
                            '            <br />\n' +
                            '            <label for="phone-number-' + storeID + '">Phone Number: </label>\n' +
                            '            <input type="text" name="phone-number-' + storeID + '" id="phone-number-' + storeID + '" value="' + storePhone + '"/>\n' +
                            '            <button onclick="sendEnableTwilio(\'' + url + '\', \'' + token + '\', ' + storeID + ')">Enable Twilio</button>\n' +
                            '            <br />\n' +
                            '    <div>\n' +
                            '        <label for="serv-hist">Service History: </label>\n' +
                            '        <select name="serv-hist" id="serv-hist-' + storeID + '" onchange="setServHistory(' + storeID + ')">\n' +
                            '            <option value="cdk">CDK</option>\n' +
                            '            <option value="dealervault">DealerVault</option>\n' +
                            '            <option value="other">Other</option>\n' +
                            '        </select>\n' +
                            '    </div>\n' +
                            '    <div id="service-history-input-' + storeID + '">\n' +
                            '    <div>\n' +
                            '        <div>\n' +
                            '            <label for="dealer-id" id="dealer-id-label-' + storeID + '">CDK 3PA ID: </label>\n' +
                            '            <input type="text" name="dealer-id" id="dealer-id-' + storeID + '" />\n' +
                            '        </div>\n' +
                            '    </div>\n' +
                            '    <div>\n' +
                            '            <button onclick="sendServiceHistory(\'' + url + '\', \'' + token + '\', ' + storeID + ')">Set Service History</button>\n' +
                            '    </div>\n' +
                            '    <div>\n' +
                            '        <div>\n' +
                            '            <label for="logo-file">Store Logo File: </label>\n' +
                            '            <input type="file" name="logo-file" id="logo-file-' + storeID + '" />\n' +
                            '        </div>\n' +
                            '    <div>\n' +
                            '            <button onclick="sendUploadLogo(\'' + url + '\', \'' + token + '\', ' + storeID + ')">Upload Store Logo</button>\n' +
                            '    </div>\n' +
                            '    </div>' +
                            '    </div>\n' +
                            '            <br />\n' +
                            '        </div>'
                    }
                    else {
                        storeList.innerHTML += '\n' +
                            '            <span>' + storeName + '</span>\n' +
                            '            <br />' +
                            '            <label for="email-address-' + storeID + '">Email Address: </label>' +
                            '            <input type="text" name="email-address-' + storeID + '" id="email-address-' + storeID + '" />' +
                            '            <button onclick="sendConfirmAvail(\'' + url + '\', \'' + token + '\', ' + storeID + ')">Confirm Availability</button>\n' +
                            '            <br />\n' +
                            '            <label for="phone-number-' + storeID + '">Phone Number: </label>\n' +
                            '            <input type="text" name="phone-number-' + storeID + '" id="phone-number-' + storeID + '" value="' + storePhone + '"/>\n' +
                            '            <button onclick="sendEnableTwilio(\'' + url + '\', \'' + token + '\', ' + storeID + ')">Enable Twilio</button>\n' +
                            '            <br />\n' +
                            '    <div>\n' +
                            '        <label for="serv-hist">Service History: </label>\n' +
                            '        <select name="serv-hist" id="serv-hist-' + storeID + '" onchange="setServHistory(' + storeID + ')">\n' +
                            '            <option value="cdk">CDK</option>\n' +
                            '            <option value="dealervault">DealerVault</option>\n' +
                            '            <option value="other">Other</option>\n' +
                            '        </select>\n' +
                            '    </div>\n' +
                            '    <div id="service-history-input-' + storeID + '">\n' +
                            '    <div>\n' +
                            '        <div>\n' +
                            '            <label for="dealer-id" id="dealer-id-label-' + storeID + '">CDK 3PA ID: </label>\n' +
                            '            <input type="text" name="dealer-id" id="dealer-id-' + storeID + '" />\n' +
                            '        </div>\n' +
                            '    </div>\n' +
                            '    <div>\n' +
                            '            <button onclick="sendServiceHistory(\'' + url + '\', \'' + token + '\', ' + storeID + ')">Set Service History</button>\n' +
                            '    </div>\n' +
                            '    <div>\n' +
                            '        <div>\n' +
                            '            <label for="logo-file">Store Logo File: </label>\n' +
                            '            <input type="file" name="logo-file" id="logo-file-' + storeID + '" />\n' +
                            '        </div>\n' +
                            '    <div>\n' +
                            '            <button onclick="sendUploadLogo(\'' + url + '\', \'' + token + '\', ' + storeID + ')">Upload Store Logo</button>\n' +
                            '    </div>\n' +
                            '    </div>' +
                            '    </div>\n' +
                            '            <br />\n' +
                            '        </div>'
                    }
                };
                emailRequest.open("GET", url + "/v1/store/" + storeID + "/confirm-avail-recipient/");
                emailRequest.setRequestHeader("Authorization", "Token " + token);
                emailRequest.send();
            }
        }
    };
    request.open("GET", storeURL);
    request.setRequestHeader("Authorization", "Token " + token);
    request.send();
}

function createCompany(url, token) {
    document.getElementById("error-message").innerText = "";
    let companyName = document.getElementById("company-name");
    let streetOne = document.getElementById("company-street-one");
    let streetTwo = document.getElementById("company-street-two");
    let city = document.getElementById("company-city");
    let state = document.getElementById("company-state");
    let zip = document.getElementById("company-zip");
    let phone = document.getElementById("company-phone");
    let fax = document.getElementById("company-fax");
    let email = document.getElementById("company-email");
    let logo = document.getElementById("company-logo");

    let companyURL = url + '/v1/store/company/1';

    let formData = new FormData();
    formData.append("company_name", companyName.value);
    formData.append('company_street_one', streetOne.value);
    formData.append('company_street_two', streetTwo.value);
    formData.append("company_city", city.value);
    formData.append("company_state", state.value);
    formData.append("company_zip", zip.value);
    formData.append("company_phone", phone.value);
    formData.append("company_fax", fax.value);
    formData.append("company_email", email.value);
    formData.append("logo_file", logo.files[0]);

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
            location.reload();
        }
        else {
            document.getElementById("error-message").innerText = "Could not create company.";
        }
    };
    request.open("PUT", companyURL);
    request.setRequestHeader("Authorization", "Token " + token);
    request.send(formData);
}

function createStore(url, token) {
    let storeType = document.getElementById("store-type").value;
    if(storeType === "0") {
        sendCreateStore(url, token, "2", false, false);
        sendCreateStore(url, token, "1", true, false);
    }
    else {
        sendCreateStore(url, token, storeType, true, true);
    }
}

function sendCreateStore(url, token, storeType, setInv, useSisterStoreID) {
    document.getElementById("error-message").innerText = "";
    let select = document.getElementById("company-select");
    let storeName = document.getElementById("store-name");
    let phone = document.getElementById("store-phone");
    let invSource = document.getElementById("inv-source");
    let imgSource = document.getElementById("img-source");
    let dms = document.getElementById("dms");
    let market = document.getElementById("market");
    let street = document.getElementById("street");
    let city = document.getElementById("city");
    let state = document.getElementById("state");
    let zip = document.getElementById("zip");
    let roVisible = document.getElementById("ro-visible");
    let sisterStoreID = document.getElementById("sister-store-id");
    let archivedDays = document.getElementById("archived-days");

    let storeURL = url + '/v1/store/onboarding/company/' + select.value + '/';

    let formData = new FormData();
    formData.append("store_name", storeName.value);
    formData.append('phone', phone.value);
    formData.append("store_type", storeType);
    formData.append("inventory_source", invSource.value);
    formData.append("image_source", imgSource.value);
    formData.append("dms_id", dms.value);
    formData.append("market_id", market.value);
    formData.append("street", street.value);
    formData.append("city", city.value);
    formData.append("state", state.value);
    formData.append("zip", zip.value);
    formData.append("ro_visible", roVisible.checked ? "1" : "0");
    if(archivedDays.value !== "") {
        formData.append("show_archived_packet_days", archivedDays.value);
    }

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
            let store = JSON.parse(request.responseText);
            if (setInv) {
                if(useSisterStoreID) {
                    sendInventory(url, token, store.company_store_id, sisterStoreID);
                }
                else {
                    sendInventory(url, token, store.company_store_id, store.company_store_id - 1);
                }
            }
            sendHistory(url, token, store.company_store_id);
            updateStores(url, token);
        }
        else {
            document.getElementById("error-message").innerText = "Could not create store";
        }
    };
    request.open("POST", storeURL);
    request.setRequestHeader("Authorization", "Token " + token)
    request.send(formData);
}

function sendInventory(url, token, store, sisterStoreID) {
    let inventoryURL = "";
    let invSource = document.getElementById("inv-source");
    let company = document.getElementById("company-select").value;
    let formData = new FormData();
    let usedID = 0;
    let newID = 0;
    switch(invSource.value) {
        case "vauto":
            inventoryURL = url + "/v1/store/onboarding/company/" + company + "/set-vauto/";
            if(document.getElementById("store-type").value == 1) {
                newID = store;
                usedID = sisterStoreID;
            }
            else if(document.getElementById("store-type").value == 2) {
                usedID = store;
                newID = sisterStoreID;
            }
            if(usedID != null && usedID !== "") {
                formData.append("used_store_id", usedID);
            }
            if(newID != null && newID !== "") {
                formData.append("new_store_id", newID);
            }
            formData.append("dealer_id", document.getElementById("inv-dealer-id").value);
            formData.append("file_name", document.getElementById("file-name").value);
            formData.append("inventory_load", document.getElementById("inventory-load").value ? "1" : "0");
            formData.append("price_change", document.getElementById("price-change").value ? "1" : "0");
            break;
        case "homenet":
            inventoryURL = url + "/v1/store/onboarding/company/" + company + "/set-homenet/";
            usedID = 0;
            newID = 0;
            if(document.getElementById("store-type").value == 1) {
                newID = store;
                usedID = sisterStoreID;
            }
            else if(document.getElementById("store-type").value == 2) {
                usedID = store;
                newID = sisterStoreID;
            }
            if(usedID != null && usedID !== "") {
                formData.append("used_store_id", usedID);
            }
            if(newID != null && newID !== "") {
                formData.append("new_store_id", newID);
            }
            formData.append("dealer_id", document.getElementById("inv-dealer-id").value);
            formData.append("file_name", document.getElementById("file-name").value);
            formData.append("inventory_load", document.getElementById("inventory-load").value ? "1" : "0");
            formData.append("price_change", document.getElementById("price-change").value ? "1" : "0");
            break;
        case "firstlook":
            inventoryURL = url + "/v1/store/onboarding/company/" + company + "/set-firstlook/";
            usedID = 0;
            newID = 0;
            if(document.getElementById("store-type").value == 1) {
                newID = store;
                usedID = sisterStoreID;
            }
            else if(document.getElementById("store-type").value == 2) {
                usedID = store;
                newID = sisterStoreID;
            }
            if(usedID != null && usedID !== "") {
                formData.append("used_store_id", usedID);
            }
            if(newID != null && newID !== "") {
                formData.append("new_store_id", newID);
            }
            formData.append("dealer_id", document.getElementById("inv-dealer-id").value);
            formData.append("file_name", document.getElementById("file-name").value);
            formData.append("inventory_load", document.getElementById("inventory-load").value ? "1" : "0");
            formData.append("price_change", document.getElementById("price-change").value ? "1" : "0");
            break;
        case "dealercom":
            inventoryURL = url + "/v1/store/onboarding/company/" + company + "/set-dealercom/";
            usedID = 0;
            newID = 0;
            if(document.getElementById("store-type").value == 1) {
                newID = store;
                usedID = sisterStoreID;
            }
            else if(document.getElementById("store-type").value == 2) {
                usedID = store;
                newID = sisterStoreID;
            }
            if(usedID != null && usedID !== "") {
                formData.append("used_store_id", usedID);
            }
            if(newID != null && newID !== "") {
                formData.append("new_store_id", newID);
            }
            formData.append("dealer_id", document.getElementById("inv-dealer-id").value);
            formData.append("inventory_load", document.getElementById("inventory-load").value ? "1" : "0");
            formData.append("price_change", document.getElementById("price-change").value ? "1" : "0");
            break;
        case "promax":
            inventoryURL = url + "/v1/store/onboarding/company/" + company + "/set-promax/";
            usedID = 0;
            newID = 0;
            if(document.getElementById("store-type").value == 1) {
                newID = store;
                usedID = sisterStoreID;
            }
            else if(document.getElementById("store-type").value == 2) {
                usedID = store
                newID = sisterStoreID;
            }
            if(usedID != null && usedID !== "") {
                formData.append("used_store_id", usedID);
            }
            if(newID != null && newID !== "") {
                formData.append("new_store_id", newID);
            }
            formData.append("dealer_id", document.getElementById("inv-dealer-id").value);
            formData.append("file_name", document.getElementById("file-name").value);
            formData.append("inventory_load", document.getElementById("inventory-load").value ? "1" : "0");
            formData.append("price_change", document.getElementById("price-change").value ? "1" : "0");
            break;
        case "cdk_global":
            inventoryURL = url + "/v1/store/onboarding/store/" + store + "/set-cdk-global/";
            formData.append("dealer_id", document.getElementById("inv-dealer-id").value);
            formData.append("file_name", document.getElementById("file-name").value);
            formData.append("inventory_load", document.getElementById("inventory-load").value ? "1" : "0");
            formData.append("price_change", document.getElementById("price-change").value ? "1" : "0");
            break;
        default:
            return;
    }

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
        } else {
            document.getElementById("error-message").innerText += "Could not set inventory provider.";
        }
    };
    request.open("POST", inventoryURL);
    request.setRequestHeader("Authorization", "Token " + token);
    request.send(formData);
}

function sendHistory(url, token, store) {
    let historySource = document.getElementById("hist-source").value;
    if(historySource === "carfax") {
        sendCarfax(url, token, store);
    }
    else if(historySource === "autocheck") {
        sendAutocheck(url, token, store);
    }
}

function sendCarfax(url, token, store) {
    let carfaxURL = url + "/v1/store/onboarding/store/" + store + "/set-carfax/";

    let formData = new FormData();
    formData.append("module_id", document.getElementById("carfax-module-id").value);
    formData.append("username", document.getElementById("carfax-user").value);
    formData.append("password", document.getElementById("carfax-pass").value);
    formData.append("dealership-contact", document.getElementById("dealership-contact").value);

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
        } else {
            document.getElementById("error-message").innerText += "Could not set Carfax.";
        }
    };
    request.open("POST", carfaxURL);
    request.setRequestHeader("Authorization", "Token " + token)
    request.send(formData);
}

function sendAutocheck(url, token, store) {
    let carfaxURL = url + "/v1/store/onboarding/store/" + store + "/set-autocheck/";

    let formData = new FormData();
    formData.append("module_id", document.getElementById("autocheck-module-id").value);
    formData.append("cid", document.getElementById("cid").value);
    formData.append("sid", document.getElementById("sid").value);
    formData.append("pwd", document.getElementById("pwd").value);

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
        } else {
            document.getElementById("error-message").innerText += "Could not set Autocheck.";
        }
    };
    request.open("POST", carfaxURL);
    request.setRequestHeader("Authorization", "Token " + token)
    request.send(formData);
}

function sendServiceHistory(url, token, store) {
    let serviceHistoryURL = "";
    let serviceHistory = document.getElementById("serv-hist-" + store).value;
    if(serviceHistory === "cdk") {
        serviceHistoryURL = url + "/v1/store/onboarding/store/" + store + "/set-cdk/";
    }
    else if(serviceHistory === "dealervault") {
        serviceHistoryURL = url + "/v1/store/onboarding/store/" + store + "/set-dealer-id/";
    }
    else {
        return;
    }

    let formData = new FormData();
    formData.append("dealer-id", document.getElementById("dealer-id-" + store).value);

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
        } else {
            document.getElementById("error-message").innerText += "Could not set service history.";
        }
    };
    request.open("POST", serviceHistoryURL);
    request.setRequestHeader("Authorization", "Token " + token)
    request.send(formData);
}

function sendConfirmAvail(url, token, store) {
    let confirmAvailURL = url + "/v1/store/onboarding/store/" + store + "/confirm-availability/";

    let formData = new FormData();
    formData.append("email-address", document.getElementById("email-address-" + store).value);
    formData.append("email-format", "1");

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
        } else {
            document.getElementById("error-message").innerText = "Could not Confirm Availability.";
        }
    };
    request.open("POST", confirmAvailURL);
    request.setRequestHeader("Authorization", "Token " + token)
    request.send(formData);
}

function sendEnableTwilio(url, token, store) {
    let twilioURL = url + "/v1/store/onboarding/store/" + store + "/enable-twilio/";

    let formData = new FormData();
    formData.append("twilio-phone", document.getElementById("phone-number-" + store).value);

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
        } else {
            document.getElementById("error-message").innerText = "Could not enable Twilio.";
        }
    };
    request.open("POST", twilioURL);
    request.setRequestHeader("Authorization", "Token " + token)
    request.send(formData);
}

function sendUploadLogo(url, token, store) {
    let logoUploadURL = url + "/v1/store/" + store + "/logo/";

    let formData = new FormData();
    formData.append("file", document.getElementById("logo-file-" + store).files[0]);

    let request = new XMLHttpRequest();
    request.onload = () => {
        if (request.status === 200) {
        } else {
            document.getElementById("error-message").innerText = "Could not enable Twilio.";
        }
    };
    request.open("PUT", logoUploadURL);
    request.setRequestHeader("Authorization", "Token " + token)
    request.send(formData);
}

function onLoad(url, token) {
    updateStores(url, token);
    setInventory();
    setHistory();
}

function setInventory() {
    let select = document.getElementById("inv-source");
    switch(select.value) {
        case "vauto":
        case "homenet":
        case "firstlook":
        case "promax":
            document.getElementById("inv-source-input").style.display = "block";
            document.getElementById("sister-store-input-div").style.display = "block";
            document.getElementById("file-name-input-div").style.display = "block";
            break;
        case "dealercom":
            document.getElementById("inv-source-input").style.display = "block";
            document.getElementById("sister-store-input-div").style.display = "block";
            document.getElementById("file-name-input-div").style.display = "none";
            break;
        case "cdk_global":
            document.getElementById("inv-source-input").style.display = "block";
            document.getElementById("sister-store-input-div").style.display = "none";
            document.getElementById("file-name-input-div").style.display = "block";
            break;
        default:
            document.getElementById("inv-source-input").style.display = "none";
            document.getElementById("sister-store-input-div").style.display = "block";
            document.getElementById("file-name-input-div").style.display = "block";
            break;
    }
}

function setHistory() {
    let select = document.getElementById("hist-source");
    if(select.value === "carfax") {
        document.getElementById("carfax-input").style.display = "block";
        document.getElementById("autocheck-input").style.display = "none";
    }
    else if (select.value === "autocheck") {
        document.getElementById("carfax-input").style.display = "none";
        document.getElementById("autocheck-input").style.display = "block";
    }
}

function setServHistory(store) {
    let select = document.getElementById("serv-hist-" + store);
    if(select.value === "other") {
        document.getElementById("serv-hist-input-" + store).style.display = "none";
    }
    else if(select.value === "cdk") {
        document.getElementById("dealer-id-label-" + store).innerText = "CDK 3PA ID: ";
        document.getElementById("serv-hist-input-" + store).style.display = "block";
    }
    else {
        document.getElementById("dealer-id-label-" + store).innerText = "Service History Dealer ID: ";
        document.getElementById("serv-hist-input-" + store).style.display = "block";
    }
}